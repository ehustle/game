# Game

## Installation

#### Server

Go in the `server` directory and type:

```
go build
```

That's it! Just run it with:

```
./server
```

The server should be running at http://127.0.0.1:8000

#### Client

Make sure you have node and npm installed.

Then go in the `client` directory and type:

```
npm i
```

Then install webpack and webpack-cli:

```
npm i -g webpack webpack-cli
```

Then type:

```
webpack --watch
```

The code is in `client/src/index.js`.

That's it!