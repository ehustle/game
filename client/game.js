    super(ctx, new Flatten.Polygon([
      [opts.x, opts.y],
      [opts.x + opts.width, opts.y],
      [opts.x + opts.width, opts.y + opts.height],
      [opts.x, opts.y + opts.height],
    ]), opts);
    this.width = opts.width;
    this.height = opts.height;
    this.texture = opts.texture;
    this.player = opts.player;
    this.camera = opts.camera;
    const gl = this.hudBitmap;
    this.texPosition = opts.texPosition;
    this.uniforms = {
      inTexCoord: { value: this.texPosition },
      texture1: { value: this.texture },
      textureSize_: { value: new Float32Array([1064, 1080]) },
      rotation: { value: 0 },
    }
    this.shaderMaterial = new THREE.ShaderMaterial({
      vertexShader: `
      uniform vec2 textureSize_;
      uniform vec2 inTexCoord;
      uniform float rotation;
      varying mediump vec2 texCoord;
      varying mediump vec2 center;
  
      void main() {
        vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
        gl_Position = projectionMatrix * modelViewPosition;
        
        float inX = (inTexCoord.x/textureSize_.x);
        float inY = (inTexCoord.y/textureSize_.y);

        float x = position.x*2.5/textureSize_.x+inX;
        float y = position.y*2.5/textureSize_.y+inY;
        
        float s = sin(rotation);
        float c = cos(rotation);

        float dX = x - inX;
        float dY = y - inY;
        
        float newX = inX + c * dX - s * dY;
        float newY = inY + s * dX + c * dY;

        center.x = inTexCoord.x/textureSize_.x;
        center.y = inTexCoord.y/textureSize_.y;
        texCoord.x = newX;
        texCoord.y = newY;
      }
      `,
      fragmentShader: `
      uniform vec2 textureSize_;
      uniform sampler2D texture1;
      varying mediump vec2 texCoord;
      varying mediump vec2 center;
      void main() {
        float dx = abs(texCoord.x - center.x);
        float dy = abs(texCoord.y - center.y);
        float dist = sqrt(dx * dx + dy * dy);
        float radius = 0.25;
        if(dist > radius)
          discard;
        else if(dist > radius-0.005)
          gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        else
          gl_FragColor = vec4(texture2D(texture1, texCoord).rgb, 1.0);
      }
      `,
      uniforms: this.uniforms,
    });
  }

      const mapWidth = 117.207;
    const mapHeight = 118.968;

    this.minimapGeometry = new THREE.PlaneBufferGeometry(this.scale2dWidth(350), this.scale2dHeight(350));
    this.minimapPlane = new THREE.Mesh(this.minimapGeometry, this.minimapElement.shaderMaterial);
    this.minimapPlane.position.x = (this.width / 2) - this.scale2dWidth(350 / 2);
    this.minimapPlane.position.y = (this.height / 2) - this.scale2dHeight(350 / 2);

    this.sceneHUD.add(this.minimapPlane);
