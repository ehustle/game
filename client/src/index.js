import * as THREE from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { clone as SkeletonClone } from './SkeletonUtils.js';
import * as Flatten from '@flatten-js/core';
import TWEEN from '@tweenjs/tween.js';
import Stats from 'stats.js';

const distance = (x1, y1, z1, x2, y2, z2) => Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2));
const isPowerOf2 = (value) => value & (value - 1) === 0;

class Item {
  constructor(id) {
    this.id = id;
    this.element = undefined;
  }
}

class HPBar {
  constructor(player, camera, startingHitsPercent = 1) {
    this.camera = camera;
    this.player = player;

    const greenMeshGeometry = new THREE.PlaneGeometry(1, 0.2);
    const greenMeshMaterial = new THREE.MeshBasicMaterial({ color: 0x00FF00 });
    greenMeshMaterial.depthTest = false;

    this.greenMesh = new THREE.Mesh(greenMeshGeometry, greenMeshMaterial);
    this.greenMesh.renderOrder = 1;

    const redMeshGeometry = new THREE.PlaneGeometry(1, 0.2);
    const redMeshMaterial = new THREE.MeshBasicMaterial({ color: 0xFF0000 });

    redMeshMaterial.depthTest = false;

    this.redMesh = new THREE.Mesh(redMeshGeometry, redMeshMaterial);
    this.redMesh.renderOrder = 0;

    this.update(startingHitsPercent);
  }

  update(hitsPercent) {
    this.hpPercent = hitsPercent

    this.greenMesh.scale.set(hitsPercent, 1, 1);

    const width = hitsPercent * 1
    const offset = -0.5 + (width / 2);

    //this.greenMesh.geometry.translate(offset, 0, 0);
    // the division here is a hack to account for the scaling not having taken place yet
    this.greenMesh.geometry.applyMatrix4(new THREE.Matrix4().makeTranslation(offset / this.hpPercent, 0, 0));
  }

  faceCamera() {
    this.greenMesh.quaternion.copy(this.camera.quaternion);
    this.redMesh.quaternion.copy(this.camera.quaternion);
  }

  center() {
    this.greenMesh.position.copy(this.player.pos);
    this.redMesh.position.copy(this.player.pos);
    this.greenMesh.position.y += 2.6;
    this.redMesh.position.y += 2.6;
    this.redMesh.quaternion.copy(this.camera.quaternion);
    this.greenMesh.quaternion.copy(this.camera.quaternion);
  }
}

class UIComponent {
  constructor(poly, opts) {
    this.x = opts.x;
    this.y = opts.y;
    this.highlighted = false;
    this.poly = poly;
    this.removed = false;

    if (opts.onMouseClick) {
      this.onMouseClick = opts.onMouseClick;
    }
    if (opts.onMouseMove) {
      this.onMouseMove = opts.onMouseMove;
    }
  }

  removeElement() {
    this.removed = true;
  }

  intersects(pt) {
    if (this.poly === null) {
      return false;
    }
    return this.poly.intersect(pt).length > 0;
  }
}

function createProgram(gl, vertexShaderSource, fragmentShaderSource) {
  let vsh = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vsh, vertexShaderSource);
  gl.compileShader(vsh);
  if (!gl.getShaderParameter(vsh, gl.COMPILE_STATUS)) {
    throw new Error("Error in vertex shader:  " + gl.getShaderInfoLog(vsh));
  }
  let fsh = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fsh, fragmentShaderSource);
  gl.compileShader(fsh);
  if (!gl.getShaderParameter(fsh, gl.COMPILE_STATUS)) {
    throw new Error("Error in fragment shader:  " + gl.getShaderInfoLog(fsh));
  }
  let prog = gl.createProgram();
  gl.attachShader(prog, vsh);
  gl.attachShader(prog, fsh);
  gl.linkProgram(prog);
  if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
    throw new Error("Link error in program:  " + gl.getProgramInfoLog(prog));
  }
  return prog;
}

class TexturedRectComponent extends UIComponent {
  constructor(gl, opts) {
    super(new Flatten.Polygon([
      [opts.x, opts.y],
      [opts.x + opts.width, opts.y],
      [opts.x + opts.width, opts.y + opts.height],
      [opts.x, opts.y + opts.height],
    ]), opts);
    this.width = opts.width;
    this.height = opts.height;
    this.textures = new Array(opts.textures.length);
    this.prog = opts.program;

    const level = 0;
    const internalFormat = gl.RGBA;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;

    gl.useProgram(this.prog);
    for (let i = 0; i < this.textures.length; i++) {
      this.textures[i] = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, this.textures[i]);
      gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
        srcFormat, srcType, opts.textures[i]);
      if (isPowerOf2(this.width) && isPowerOf2(this.height)) {
        // Yes, it's a power of 2. Generate mips.
        gl.generateMipmap(gl.TEXTURE_2D);
      } else {
        // No, it's not a power of 2. Turn off mips and set
        // wrapping to clamp to edge
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      }
    }
    this.currentTexture = this.textures[0];
    this.a_coords_location = gl.getAttribLocation(this.prog, "a_coords");      // location of the "a_coords" attribute variable
    this.a_coords_buffer = gl.createBuffer();        // a VBO to hold the vertex coordinates
    this.a_texCoords_location = gl.getAttribLocation(this.prog, "a_texCoords");;   // location of the "a_texCoords" attribute variable
    this.a_texCoords_buffer = gl.createBuffer();     // a VBO to hold the texture coordinates

    this.u_texture_location = gl.getUniformLocation(this.prog, "u_texture");     // the location of the "u_texture" uniform variable

    const width = (this.width / window.innerWidth) * 2;
    const height = (this.height / window.innerHeight) * 2;
    const x = (this.x / window.innerWidth) * 2 - 1;
    const y = (-this.y / window.innerHeight) * 2 + 1;

    this.coords = new Float32Array([
      x, y,
      x + width, y,
      x + width, y - height,
      x, y - height
    ]);

    this.texCoords = new Float32Array([0, 0, 1, 0, 1, 1, 0, 1]);
  }

  recreate(x, y, width, height) {
    this.poly = new Flatten.Polygon([
      [x, y],
      [x + width, y],
      [x + width, y + height],
      [x, y + height],
    ]);
  }

  setCurrentTexture(idx) {
    this.currentTexture = this.textures[idx];
  }

  draw(gl) {
    gl.useProgram(this.prog);

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.a_coords_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.coords, gl.STREAM_DRAW);
    gl.vertexAttribPointer(this.a_coords_location, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(this.a_coords_location);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.a_texCoords_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.texCoords, gl.STREAM_DRAW);
    gl.vertexAttribPointer(this.a_texCoords_location, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(this.a_texCoords_location);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.currentTexture);
    gl.uniform1i(this.u_texture_location, 0);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  }
}

class MinimapComponent extends UIComponent {
  constructor(gl, opts) {
    super(Flatten.circle(Flatten.point(opts.x+opts.width/2, opts.y+opts.height/2), opts.width/2), opts);
    this.width = opts.width;
    this.height = opts.height;
    this.player = opts.player;
    this.camera = opts.camera;

    this.inTexCoord = new Float32Array([0, 0]);
    this.textureSize_ = new Float32Array([1064, 1080]);
    this.rotation = 0;
    this.dimensions = new Float32Array([this.width, this.height]);
    this.borderWidth = 0.003;

    this.prog = opts.program;

    gl.useProgram(this.prog);

    const level = 0;
    const internalFormat = gl.RGBA;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;

    this.texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
      srcFormat, srcType, opts.texture);
    if (isPowerOf2(this.width) && isPowerOf2(this.height)) {
      // Yes, it's a power of 2. Generate mips.
      gl.generateMipmap(gl.TEXTURE_2D);
    } else {
      // No, it's not a power of 2. Turn off mips and set
      // wrapping to clamp to edge
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }

    this.positionUni = gl.getUniformLocation(this.prog, "position1");
    this.inTexCoordUni = gl.getUniformLocation(this.prog, "inTexCoord");
    this.textureSizeUni = gl.getUniformLocation(this.prog, "textureSize_");;
    this.rotationUni = gl.getUniformLocation(this.prog, "rotation");
    this.texture1Uni = gl.getUniformLocation(this.prog, "texture1");
    this.dimensionsUni = gl.getUniformLocation(this.prog, "dimensions");
    this.borderWidthUni = gl.getUniformLocation(this.prog, "borderWidth");
    this.coordsAttrib = gl.getAttribLocation(this.prog, "coords1");
    this.coordsBuffer = gl.createBuffer();
    this.coords2Attrib = gl.getAttribLocation(this.prog, "coords2");
    this.coords2Buffer = gl.createBuffer();

    gl.uniform2fv(this.inTexCoordUni, this.inTexCoord);
    gl.uniform2fv(this.textureSizeUni, this.textureSize_);
    gl.uniform1f(this.rotationUni, this.rotation);
    gl.uniform1i(this.texture1Uni, this.texture);
    gl.uniform2fv(this.dimensionsUni, this.dimensions);
    gl.uniform1f(this.borderWidthUni, this.borderWidth);

    const width = (this.width / window.innerWidth) * 2;
    const height = (this.height / window.innerHeight) * 2;
    const x = (this.x / window.innerWidth) * 2 - 1;
    const y = (this.y / window.innerHeight) * 2 + 1;
    this.coords = new Float32Array([
      x, y,
      x + width, y,
      x + width, y - height,
      x, y - height
    ]);

    this.texCoords = new Float32Array([
      (this.x  / window.innerWidth),
      (this.y  / window.innerHeight)
    ]);
  /*
    super(gl, new Flatten.Polygon([
      [opts.x, opts.y],
      [opts.x + opts.width, opts.y],
      [opts.x + opts.width, opts.y + opts.height],
      [opts.x, opts.y + opts.height],
    ]), opts);
    this.width = opts.width;
    this.height = opts.height;
    this.texture = opts.texture;
    this.player = opts.player;
    this.camera = opts.camera;
    this.texPosition = opts.texPosition;
    this.uniforms = {
      inTexCoord: { value: this.texPosition },
      texture1: { value: this.texture },
      textureSize_: { value: new Float32Array([1064, 1080]) },
      rotation: { value: 0 },
    }
    this.shaderMaterial = new THREE.ShaderMaterial({
      vertexShader: `
      uniform vec2 textureSize_;
      uniform vec2 inTexCoord;
      uniform float rotation;
      varying mediump vec2 texCoord;
      varying mediump vec2 center;
  
      void main() {
        vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
        gl_Position = projectionMatrix * modelViewPosition;
        
        float inX = (inTexCoord.x/textureSize_.x);
        float inY = (inTexCoord.y/textureSize_.y);

        float x = position.x*2.5/textureSize_.x+inX;
        float y = position.y*2.5/textureSize_.y+inY;
        
        float s = sin(rotation);
        float c = cos(rotation);

        float dX = x - inX;
        float dY = y - inY;
        
        float newX = inX + c * dX - s * dY;
        float newY = inY + s * dX + c * dY;

        center.x = inTexCoord.x/textureSize_.x;
        center.y = inTexCoord.y/textureSize_.y;
        texCoord.x = newX;
        texCoord.y = newY;
      }
      `,
      fragmentShader: `
      uniform vec2 textureSize_;
      uniform sampler2D texture1;
      varying mediump vec2 texCoord;
      varying mediump vec2 center;
      void main() {
        float dx = abs(texCoord.x - center.x);
        float dy = abs(texCoord.y - center.y);
        float dist = sqrt(dx * dx + dy * dy);
        float radius = 0.25;
        if(dist > radius)
          discard;
        else if(dist > radius-0.005)
          gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        else
          gl_FragColor = vec4(texture2D(texture1, texCoord).rgb, 1.0);
      }
      `,
      uniforms: this.uniforms,
    });*/
  }

  draw(gl) {
    gl.useProgram(this.prog);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, this.coords2Buffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.texCoords, gl.STREAM_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.coordsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.coords, gl.STREAM_DRAW);
    gl.vertexAttribPointer(this.coordsAttrib, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(this.coordsAttrib);

    gl.uniform2fv(this.inTexCoordUni, this.inTexCoord);
    gl.uniform1f(this.rotationUni, this.rotation);
    gl.uniform2fv(this.dimensionsUni, this.dimensions);
    gl.uniform1f(this.borderWidthUni, this.borderWidth);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  }
}

class DebugMenu extends UIComponent {
  constructor(opts) {
    super(null, opts);
    this.player = opts.player;
  }

  draw(gl) {
    /*  this.ctx.font = "Normal 12px Arial";
      this.ctx.fillStyle = "rgba(245,245,245,0.75)";
      this.ctx.fillText('(' + (this.player.pos.x).toFixed(4) + ',' + (this.player.pos.y).toFixed(4) + ',' + (this.player.pos.z).toFixed(4) + ')', this.x, this.y);
      */
  }
}

class ClickX extends UIComponent {
  constructor(gl, opts) {
    super(null, opts);

    this.visible = false;
    this.textures = new Array(opts.textures.length);
    this.prog = opts.program;

    const level = 0;
    const internalFormat = gl.RGBA;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;
    gl.useProgram(this.prog);

    for (let i = 0; i < this.textures.length; i++) {
      this.textures[i] = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, this.textures[i]);
      gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
        srcFormat, srcType, opts.textures[i]);
      if (isPowerOf2(this.width) && isPowerOf2(this.height)) {
        // Yes, it's a power of 2. Generate mips.
        gl.generateMipmap(gl.TEXTURE_2D);
      } else {
        // No, it's not a power of 2. Turn off mips and set
        // wrapping to clamp to edge
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      }
    }
    this.currentTexture = this.textures[0];

    this.a_coords_location = gl.getAttribLocation(this.prog, "a_coords");      // location of the "a_coords" attribute variable
    this.a_coords_buffer = gl.createBuffer();        // a VBO to hold the vertex coordinates
    this.a_texCoords_location = gl.getAttribLocation(this.prog, "a_texCoords");;   // location of the "a_texCoords" attribute variable
    this.a_texCoords_buffer = gl.createBuffer();     // a VBO to hold the texture coordinates

    this.u_texture_location = gl.getUniformLocation(this.prog, "u_texture");     // the location of the "u_texture" uniform variable

    this.texCoords = new Float32Array([0, 0, 1, 0, 1, 1, 0, 1]);
  }
  start(x1, y1, mode) {
    this.x = x1;
    this.y = y1;
    this.centerY = y1;
    this.width = 25;
    this.height = 25;
    this.mode = mode;
    this.visible = true;

    const width = (this.width / window.innerWidth) * 2;
    const height = (this.height / window.innerHeight) * 2;
    const x = ((this.x / window.innerWidth) * 2 - 1) - width / 2;
    const y = ((-this.y / window.innerHeight) * 2 + 1) + height / 2;

    this.coords = new Float32Array([
      x, y,
      x + width, y,
      x + width, y - height,
      x, y - height
    ]);

    const scale = { weight: 1 };
    if (this.runningTween) {
      this.runningTween.stop();
    }
    this.runningTween = new TWEEN.Tween(scale)
      .to({ weight: 0 }, 600)
      .easing(TWEEN.Easing.Linear.None)
      .onUpdate(() => {
        this.coords[0] = x;
        this.coords[1] = y;
        this.coords[2] = (x + width * scale.weight);
        this.coords[3] = y;
        this.coords[4] = (x + width * scale.weight);
        this.coords[5] = (y - height * scale.weight);
        this.coords[6] = x;
        this.coords[7] = (y - height * scale.weight);

        for(let i = 0; i < this.coords.length; i += 2) {
          this.coords[i] += width / 2 - (this.coords[2] - this.coords[0]) / 2;
          this.coords[i+1] -= height / 2 + (this.coords[5] - this.coords[1]) / 2;
        }
      })
      .onComplete(() => {
        this.runningTween = undefined
        this.visible = false;
      })
      .start();
  }
  draw(gl) {
    if (this.visible) {
      gl.useProgram(this.prog);

      gl.enable(gl.BLEND);
      gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

      gl.bindBuffer(gl.ARRAY_BUFFER, this.a_coords_buffer);
      gl.bufferData(gl.ARRAY_BUFFER, this.coords, gl.STREAM_DRAW);
      gl.vertexAttribPointer(this.a_coords_location, 2, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(this.a_coords_location);

      gl.bindBuffer(gl.ARRAY_BUFFER, this.a_texCoords_buffer);
      gl.bufferData(gl.ARRAY_BUFFER, this.texCoords, gl.STREAM_DRAW);
      gl.vertexAttribPointer(this.a_texCoords_location, 2, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(this.a_texCoords_location);

      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this.textures[this.mode]);
      gl.uniform1i(this.u_texture_location, 0);

      gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    }
  }
}

class Inventory {
  constructor(items) {
    this.items = items;
    this.uiElement = new TexturedRectComponent
  }
}

class Player {
  static IDLE_ANIM = 0;
  static WALK_ANIM = 1;
  static ATTACK_ANIM = 2;

  constructor(model, playerID, camera) {
    const p = SkeletonClone(model);

    this.pos = p.position;
    this.model = p;
    this.model.scale.set(0.01, 0.01, 0.01);

    this.pid = playerID;

    this.clips = [];

    const mixer = new THREE.AnimationMixer(p);
    const clipIdle = THREE.AnimationClip.findByName(p.animations, 'metarig|Idle');
    this.clips[Player.IDLE_ANIM] = { enabled: false, clip: mixer.clipAction(clipIdle) };
    const clipWalking = THREE.AnimationClip.findByName(p.animations, 'metarig|Walk');
    this.clips[Player.WALK_ANIM] = { enabled: false, clip: mixer.clipAction(clipWalking) };
    const clipAttack = THREE.AnimationClip.findByName(p.animations, 'metarig|Attack_2');
    this.clips[Player.ATTACK_ANIM] = { enabled: false, clip: mixer.clipAction(clipAttack) };

    const clock = new THREE.Clock(true);
    this.updater = () => {
      mixer.update(clock.getDelta());
    }

    this.hpBar = new HPBar(this, camera, 0.75);
  }

  enableAnim(anim) {
    for (let i = 0; i < this.clips.length; i++) {
      if (i === anim) {
        if (this.clips[i].enabled) {
          continue;
        }
        this.clips[i].clip.play();
        this.clips[i].enabled = true;
      } else {
        if (this.clips[i].enabled) {
          this.clips[i].clip.stop();
          this.clips[i].enabled = false;
        }
      }
    }
  }

  walkDelay(pos) {
    return 400 * (distance(this.pos.x, this.pos.y, this.pos.z, pos.x, pos.y, pos.z) / (this.speed * 1.25));
  }
}

class Client {
  static minPolarAngle = 0; // radians
  static maxPolarAngle = Math.PI; // radians
  static minDistance = 0;
  static maxDistance = Infinity;

  constructor(width, height) {
    this.width = width;
    this.height = height;

    this.mouse = new THREE.Vector2();
    this.spherical = new THREE.Spherical();
    this.sphericalDelta = new THREE.Spherical();
    this.rotateStart = new THREE.Vector2();
    this.rotateEnd = new THREE.Vector2();
    this.rotateDelta = new THREE.Vector2();
    this.mouseWindow = new Flatten.Point(0, 0);

    this.collideableObjects = [];
    this.players = new Map();
    this.playerIDsLastUpdate = [];

    this.clicked = false;

    this.uiElements = [];
  }

  rotateCameraLeft(angle) {
    this.sphericalDelta.theta -= angle;
  }

  rotateCameraUp(angle) {
    this.sphericalDelta.phi -= angle;
  }

  setupRenderer() {
    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(45, this.width / this.height, 1, 1000);

    this.renderer = new THREE.WebGLRenderer({
      antialias: true
    });

    this.renderer.autoClear = false;
    this.renderer.setSize(this.width, this.height);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    /*this.mapCamera = new THREE.OrthographicCamera(
      this.width / -2,		// Left
      this.width / 2,		// Right
      this.height / 2,		// Top
      this.height / -2,	// Bottom
      -500,            			// Near 
      1000 );           			// Far 
    this.mapCamera.up = new THREE.Vector3(0,0,-1);
    this.mapCamera.lookAt( new THREE.Vector3(0,-1,0) );
    this.mapCamera.zoom = 5.5;
    this.mapCamera.updateProjectionMatrix();*/
    //this.scene.add(this.mapCamera);

    this.raycaster = new THREE.Raycaster();

    this.cameraOffset = new THREE.Vector3();

    // so camera.up is the orbit axis
    this.quat = new THREE.Quaternion().setFromUnitVectors(this.camera.up, new THREE.Vector3(0, 1, 0));
    this.quatInverse = this.quat.clone().invert();

    document.body.appendChild(this.renderer.domElement);

    this.stats = Stats();
    document.body.appendChild(this.stats.dom);
  }

  loadHUDShaders(gl) {
    this.PROG_TEXTURED_RECT = createProgram(gl, `
attribute vec2 a_coords;
attribute vec2 a_texCoords;
varying vec2 v_texCoords;
void main() {
   v_texCoords = a_texCoords;
   gl_Position = vec4(a_coords, 0.0, 1.0);
}
`, `       
precision mediump float;
uniform sampler2D u_texture;
varying vec2 v_texCoords;
void main() {
   vec4 color = texture2D( u_texture, v_texCoords );
   gl_FragColor = color;
}
`);
    this.PROG_MINIMAP = createProgram(gl, `
    precision mediump float;

    uniform vec2 textureSize_;
    uniform vec2 inTexCoord;
    uniform float rotation;
    varying vec2 texCoord;
    varying vec2 center;
    
    attribute vec2 coords1;
    attribute vec2 coords2;
    void main() {
      gl_Position = vec4(coords1, 0.0, 1.0);
    
      float inX = (inTexCoord.x/textureSize_.x);
      float inY = (inTexCoord.y/textureSize_.y);
    
      float x = coords2.x/2.0;
      float y = coords2.y/2.0;

      float s = sin(-rotation);
      float c = cos(-rotation);
    
      float dX = x - 0.25;
      float dY = y - 0.25;
          
      float newX = inX + c * dX - s * dY;
      float newY = inY + s * dX + c * dY;
    
      center.x = inTexCoord.x/textureSize_.x;
      center.y = inTexCoord.y/textureSize_.y;
      texCoord.x = newX;
      texCoord.y = newY;
    }
        `, `
    
    precision mediump float;
    
    uniform vec2 textureSize_;
    uniform sampler2D texture1;
    uniform vec2 dimensions;
    uniform float borderWidth;
    varying vec2 texCoord;
    varying vec2 center;
    void main() {
      float dx = texCoord.x - center.x;
      float dy = texCoord.y - center.y;
      float dist = dx * dx + dy * dy;
      float radius = pow((dimensions.x*2.5/textureSize_.x)/2.0-borderWidth, 2.0);
      if(dist > radius)
        discard;
      else if(dist > radius-borderWidth)
        gl_FragColor = vec4(1.0, 1.0, 1.0, 0.3);
      else if(texCoord.x > 1.0 || texCoord.x < 0.0 || texCoord.y > 1.0 || texCoord.y < 0.0)
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
      else
        gl_FragColor = vec4(texture2D(texture1, texCoord).rgb, 1.0);
     }
    `)
  }

  setupHUD() {
    // We will use 2D canvas element to render our HUD.  
    this.hudCanvas = document.createElement("canvas");

    this.hudCanvas.width = this.width;
    this.hudCanvas.height = this.height;

    this.hudBitmap = this.hudCanvas.getContext('webgl');

    this.cameraHUD = new THREE.OrthographicCamera(-this.width / 2, this.width / 2, this.height / 2, -this.height / 2, 0, 30);

    this.sceneHUD = new THREE.Scene();

    this.hudTexture = new THREE.Texture(this.hudCanvas)
    this.hudTexture.needsUpdate = true;

    const material = new THREE.MeshBasicMaterial({ map: this.hudTexture });
    material.transparent = true;

    const planeGeometry = new THREE.PlaneGeometry(this.width, this.height);
    const plane = new THREE.Mesh(planeGeometry, material);

    this.sceneHUD.add(plane);

    this.loadHUDShaders(this.hudBitmap);
  }

  scale2dX(x) {
    return x * Math.min(1, this.width / 1000);
  }

  scale2dY(y) {
    return y * Math.min(1, this.height / 1000);
  }

  scale2dWidth(w) {
    return w * Math.min(1, Math.min(this.width, this.height) / 1000);
  }

  scale2dHeight(h) {
    return h * Math.min(1, Math.min(this.width, this.height) / 1000);
  }

  createInventoryUI() {
    const inventoryWidth = this.scale2dWidth(360);
    const inventoryHeight = this.scale2dWidth(500);

    this.inventoryUI = new TexturedRectComponent(this.hudBitmap, {
      x: this.width - inventoryWidth,
      y: this.height - this.scale2dHeight(100) - inventoryHeight,
      width: inventoryWidth,
      height: inventoryHeight,
      textures: [this.sprites[4]],
      program: this.PROG_TEXTURED_RECT,
      onMouseClick: event => {
        console.log('inventory mouse click');
      }
    });
    this.uiElements.push(this.inventoryUI);

    const inventoryOffsetX = this.scale2dWidth(10);
    const inventoryOffsetY = this.scale2dHeight(10);
    const slotWidth = (inventoryWidth - inventoryOffsetX * 2) / 4;
    const slotHeight = (inventoryHeight - inventoryOffsetY * 2) / 8;
    for (let i = 0; i < this.inventory.length; i++) {
      const item = this.inventory[i];
      if (item === undefined) {
        continue;
      }
      this.inventory[i].element = new TexturedRectComponent(this.hudBitmap, {
        x: this.width - inventoryWidth + inventoryOffsetX + (slotWidth * (i % 4)),
        y: this.height - this.scale2dHeight(600) + inventoryOffsetY + (slotHeight * Math.floor(i / 4)),
        width: slotWidth,
        height: slotHeight,
        textures: [this.itemTextures[item.id]],
        program: this.PROG_TEXTURED_RECT,
        onMouseClick: event => {
          console.log('item #' + i + ' used');
          this.socket.send('USEITEM ' + i);
        }
      });
      this.uiElements.push(this.inventory[i].element);
    }
  }

  removeInventoryUI() {
    if (this.inventoryUI) {
      this.inventoryUI.removeElement();
      this.inventoryUI = undefined;
      for (const item of this.inventory) {
        if (item && item.element) {
          item.element.removeElement();
          item.element = undefined;
        }
      }
    }
  }

  putInventorySlot(slot, item) {
    this.inventory[slot] = item;

    if (!this.inventoryUI) return;

    const inventoryOffsetX = this.scale2dWidth(10);
    const inventoryOffsetY = this.scale2dHeight(10);
    const slotWidth = (inventoryWidth - inventoryOffsetX * 2) / 4;
    const slotHeight = (inventoryHeight - inventoryOffsetY * 2) / 8;

    this.inventory[slot].element = new TexturedRectComponent(this.hudBitmap, {
      x: this.width - inventoryWidth + inventoryOffsetX + (slotWidth * (slot % 4)),
      y: this.height - this.scale2dHeight(600) + inventoryOffsetY + (slotHeight * Math.floor(slot / 4)),
      width: slotWidth,
      height: slotHeight,
      textures: [this.itemTextures[item.id]],
      program: this.PROG_TEXTURED_RECT,
      onMouseClick: event => {
        console.log('item #' + slot + ' used');
        this.socket.send('USEITEM ' + slot);
      }
    });
    this.uiElements.push(this.inventory[slot].element);
  }

  removeInventorySlot(slot) {
    this.inventory[slot].element.removeElement();
    this.inventory[slot].element = undefined;
    this.inventory[slot] = undefined;
  }

  createStatsUI() {
    const statsWidth = this.scale2dWidth(360);
    const statsHeight = this.scale2dWidth(500);

    this.statsUI = new TexturedRectComponent(this.hudBitmap, {
      x: this.width - statsWidth,
      y: this.height - this.scale2dHeight(100) - statsHeight,
      width: statsWidth,
      height: statsHeight,
      textures: [this.sprites[5]],
      program: this.PROG_TEXTURED_RECT,
      onMouseClick: event => {
        console.log('stats mouse click');
      }
    });
    this.uiElements.push(this.statsUI);
  }

  removeStatsUI() {
    if (this.statsUI) {
      this.statsUI.removeElement();
      this.statsUI = undefined;
    }
  }

  setupControlsMenu() {
    let x = this.width - this.scale2dWidth(200),
      y = this.height - this.scale2dHeight(100);

    const active = new Array(2).fill(false);
    const icons = [];
    const swap = idx => {
      for (let i = 0; i < active.length; i++) {
        if (idx !== i) {
          active[i] = false;
          icons[i].setCurrentTexture(0);
        } else {
          active[i] = !active[i];
          icons[i].setCurrentTexture(active[i] ? 1 : 0);
        }
      }
    };

    // inventory icon
    icons.push(
      new TexturedRectComponent(this.hudBitmap, {
        x: x,
        y: y,
        width: this.scale2dWidth(100),
        height: this.scale2dHeight(100),
        textures: [this.sprites[0], this.sprites[1]],
        program: this.PROG_TEXTURED_RECT,
        onMouseClick: event => {
          swap(0);
          this.removeStatsUI();
          if (active[0]) {
            this.createInventoryUI();
          } else {
            this.removeInventoryUI();
          }
        },
        onMouseMove: event => {
          //console.log('mouse move on inventory icon');
        }
      })
    );

    // levels icon
    icons.push(
      new TexturedRectComponent(this.hudBitmap, {
        x: x + this.scale2dWidth(100),
        y: y,
        width: this.scale2dWidth(100),
        height: this.scale2dHeight(100),
        textures: [this.sprites[2], this.sprites[3]],
        program: this.PROG_TEXTURED_RECT,
        onMouseClick: event => {
          swap(1);
          this.removeInventoryUI();
          if (active[1]) {
            this.createStatsUI();
          } else {
            this.removeStatsUI();
          }
        },
        onMouseMove: event => {
          //console.log('mouse move on inventory icon');
        }
      })
    );

    icons.forEach(icon => this.uiElements.push(icon));
  }

  addLight() {
    const light = new THREE.DirectionalLight(0xffffff, 1);
    /*light.shadow.camera.left = -3000;
    light.shadow.camera.right = 3000;
    light.shadow.camera.top = 3500;
    light.shadow.camera.bottom = -3000;*/
    light.castShadow = false;

    //light.shadow.mapSize.x = 2048
    //light.shadow.mapSize.y = 2048

    this.scene.add(light);
  }


  connect(url, handlers) {
    this.socket = new WebSocket(url);

    this.socket.addEventListener('open', handlers.onOpen);
    this.socket.addEventListener('message', handlers.onMessage);
  }

  static parsePacket(packet) {
    const data = packet.split(' ');
    const command = data[0];

    return { command, data };
  }

  consumeClick() {
    this.clicked = false;
  }

  handleMouseMoveRotate(event) {
    this.rotateEnd.set(event.clientX, event.clientY);

    this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart).multiplyScalar(1 /* rotate speed */);

    if (this.rotateDelta.x === 0 && this.rotateDelta.y === 0) {
      return;
    }

    this.rotateCameraLeft(2 * Math.PI * this.rotateDelta.x / this.height); // yes, height
    this.rotateCameraUp(2 * Math.PI * this.rotateDelta.y / this.height);

    this.rotateStart.copy(this.rotateEnd);

    this.updateCamera();
  }

  onMouseClick(event) {
    switch (event.button) {
      case 0:
        this.mouseWindow.x = event.clientX
        this.mouseWindow.y = event.clientY;
        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
        this.mouse.x = (event.clientX / this.width) * 2 - 1;
        this.mouse.y = -(event.clientY / this.height) * 2 + 1;
        this.leftClicked = true;
        break;
      case 2:
        this.mouseWindow.x = event.clientX
        this.mouseWindow.y = event.clientY;
        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
        this.mouse.x = (event.clientX / this.width) * 2 - 1;
        this.mouse.y = -(event.clientY / this.height) * 2 + 1;
        this.clicked = true;
        break;
      case 1:
        this.rotateStart.set(event.clientX, event.clientY);
        this.rotate = true;
        break;
      default:
        break;
    }
  }

  onMouseUp(event) {
    switch (event.button) {
      case 0:
        this.clicked = false;
        break;
      case 2:
        this.leftClicked = false;
        break;
      case 1:
        this.rotate = false;
        break;
      default:
        break;
    }
  }

  onResize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.renderer.setSize(this.width, this.height);

    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();

    this.cameraHUD.left = -this.width / 2;
    this.cameraHUD.right = this.width / 2;
    this.cameraHUD.top = this.height / 2;
    this.cameraHUD.bottom = -this.height / 2;

    this.cameraHUD.updateProjectionMatrix();
  }

  onMouseMove(event) {
    if (this.rotate) {
      this.handleMouseMoveRotate(event);
    }
  }

  updateCamera() {
    const position = this.camera.position;

    this.cameraOffset.copy(position).sub(this.player.pos);

    // rotate offset to "y-axis-is-up" space
    this.cameraOffset.applyQuaternion(this.quat);


    // angle from z-axis around y-axis
    this.spherical.setFromVector3(this.cameraOffset);

    this.spherical.theta += this.sphericalDelta.theta;
    this.spherical.phi += this.sphericalDelta.phi;

    this.spherical.phi = Math.max(Client.minPolarAngle, Math.min(Client.maxPolarAngle, this.spherical.phi));
    this.spherical.makeSafe();
    this.spherical.radius = 100; //Math.max(Client.minDistance, Math.min(Client.maxDistance, this.spherical.radius));

    this.cameraOffset.setFromSpherical(this.spherical);

    // rotate offset back to "camera-up-vector-is-up" space
    this.cameraOffset.applyQuaternion(this.quatInverse);

    position.copy(this.player.pos).add(this.cameraOffset);

    this.camera.lookAt(this.player.pos);

    this.sphericalDelta.set(0, 0, 0);
  }

  intersectCircle(player, rayVector, yStartOffset) {
    const segments = 8;
    const radius = 25;
    const thetaStart = 0;
    const thetaLength = Math.PI * 2;

    const circle = new Array(segments);

    for (let s = 0; s <= segments; s++) {
      const segment = thetaStart + s / segments * thetaLength;
      circle[s] = new THREE.Vector3(
        radius * Math.cos(segment) + player.pos.x,
        player.originalY + yStartOffset,
        radius * Math.sin(segment) + player.pos.z,
      );
    }
    const intersects = circle.map(pt => {
      const caster = new THREE.Raycaster();
      caster.set(pt, rayVector);
      const intersections = caster.intersectObject(this.ground, true)
      if (intersections.length > 0) {
        return intersections[0];
      }
      return undefined;
    });

    let minDistance = 0xffffffff;
    let minIntersection;
    for (let i = 0; i < intersects.length; i++) {
      const intersection = intersects[i];
      if (!intersection) continue;
      const dist = distance(player.pos.x, player.originalY, player.pos.z, intersection.point.x, intersection.point.y, intersection.point.z)
      if (dist < minDistance) {
        minDistance = dist;
        minIntersection = intersection;
      }
    }

    return minIntersection;
  }

  spawnPlayer(pid, x, y, z, dir, map) {
    for(const v of map) {
      const chunk = this.ground[v.id].scene.clone();
      chunk.position.x = v.x;
      chunk.position.y = v.y;
      chunk.position.z = v.z;
      chunk.rotation.y = v.rotation;

      this.scene.add(chunk);
    }

    this.player = new Player(this.playerModel, pid, this.camera);

    this.player.pos.x = x;
    this.player.pos.y = y;
    this.player.pos.z = z;
    this.player.model.rotation.y = dir;

    this.scene.add(this.player.model);
    this.scene.add(this.player.hpBar.redMesh);
    this.scene.add(this.player.hpBar.greenMesh);


    this.updateCamera();

    this.player.clips[Player.IDLE_ANIM].clip.play();

    this.debugMenu = new DebugMenu(this.hudBitmap, {
      x: 5,
      y: 75,
      player: this.player
    });

    if (this.minimapElement) {
      const currentIndex = this.uiElements.indexOf(this.minimapElement);
      if (currentIndex !== -1) {
        this.uiElements[currentIndex].splice(currentIndex, 1);
      }
    }
    const inTexPosition = new Float32Array([0, 0]);
    this.minimapElement = new MinimapComponent(this.hudBitmap, {
      x: this.width - this.scale2dWidth(350),
      y: 0,
      width: this.scale2dWidth(350),
      height: this.scale2dHeight(350),
      texture: this.sprites[9],
      player: this.player,
      camera: this.camera,
      program: this.PROG_MINIMAP,
      //texPosition: inTexPosition,
      onMouseClick: event => {
/*        const dx = (event.x - this.minimapElement.x) - (this.scale2dWidth(350)/2);
        const dy = (event.y - this.minimapElement.y) - (this.scale2dHeight(350)/2);

        const dxv = dx * (117.207/1064) * 2;
        const dyv = dy * (118.968/1080) * 2;

        console.log('@@>', dxv, dyv);

        const xb = this.player.pos.x + dxv;
        const yb = this.player.pos.z + dyv;

        const cx = this.player.pos.x - this.camera.position.x;
        const cy = this.player.pos.z - this.camera.position.z;
        const rot = Math.atan2(-cy, cx);
        const c = Math.cos(rot);
        const s = Math.sin(rot);
        const dx2 = this.player.pos.x;
        const dy2 = this.player.pos.z;

        const rotX = c*(xb-dx2)-s*(yb-dy2)+dx2;
        const rotY = s*(xb-dx2)+c*(yb-dy2)+dy2;*/
/*
        const mapWidth = 117.207;
        const mapHeight = 118.968;
        const px = this.player.pos.x + (mapWidth / 2);
        const py = this.player.pos.z + (mapHeight / 2);
        const tw = 1064,
              th = 1080;
        const tx = (px / mapWidth) * tw + dx * 2;
        const ty = (py / mapHeight) * th + dy * 2;
        const x = (tx / tw) * mapWidth - (mapWidth / 2);
        const y = (ty / th) * mapHeight - (mapHeight / 2);

        const cx = this.player.pos.x - this.camera.position.x;
        const cy = this.player.pos.z - this.camera.position.z;
        const rot = Math.atan2(-cy, cx);
        const c = Math.cos(rot);
        const s = Math.sin(rot);
        const dx2 = this.player.pos.x;
        const dy2 = this.player.pos.z;
        const rotX = c*(x-dx2)-s*(y-dy2)+dx2;
        const rotY = s*(x-dx2)+c*(y-dy2)+dy2;
        */
        /*const raycaster = new THREE.Raycaster();
        raycaster.set(new THREE.Vector3(rotX, 1000, rotY), new THREE.Vector3(0, -1, 0));

        const intersection = raycaster.intersectObject(this.ground, true);
        if (intersection.length > 0) {
          this.socket.send('MOVETO ' + intersection[0].point.x + ' ' + intersection[0].point.y + ' ' + intersection[0].point.z);
          this.clickX.start(this.mouseWindow.x, this.mouseWindow.y, 1);
        }*/
      }
    });

    /*
    const mapWidth = 117.207;
    const mapHeight = 118.968;

    this.minimapGeometry = new THREE.PlaneBufferGeometry(this.scale2dWidth(350), this.scale2dHeight(350));
    this.minimapPlane = new THREE.Mesh(this.minimapGeometry, this.minimapElement.shaderMaterial);
    this.minimapPlane.position.x = (this.width / 2) - this.scale2dWidth(350 / 2);
    this.minimapPlane.position.y = (this.height / 2) - this.scale2dHeight(350 / 2);

    this.sceneHUD.add(this.minimapPlane);
    */
    this.uiElements.push(this.minimapElement);
  }

  updatePlayers() {
    for (const [k, v] of this.players) {
      if (!this.playerIDsLastUpdate.includes(k)) {
        this.scene.remove(v.model);
        this.scene.remove(v.hpBar.greenMesh);
        this.scene.remove(v.hpBar.redMesh);
        this.players.delete(k);
        const index = this.collideableObjects.indexOf(v.model);
        if (index !== -1) {
          this.collideableObjects.splice(index, 1);
        }
      } else {
        v.updater();
      }
    }
  }


  handleSceneLeftClick() {
    this.raycaster.setFromCamera(this.mouse, this.camera);

    const intersects = this.raycaster.intersectObjects(this.collideableObjects, true);

    let groundFound = false;
    let otherAction = false;
    let x, y, z;
    for (let i = 0; i < intersects.length; i++) {
      console.log(intersects[i].object.name)
      if (/.+_Mazmorra_.+/.test(intersects[i].object.name)) {
        groundFound = true;
        x = intersects[i].point.x;
        y = intersects[i].point.y;
        z = intersects[i].point.z;
        break;
      }/* else if(!otherAction) {
        otherAction = true;
        if(intersects[i].object.name === 'Spartan_LP') {
          socket.send('ATTACK ' + intersects[i].object.playerID);
        }
      }*/
    }
    if (groundFound && !otherAction) {
      console.log("@@FLASHING TO", x + ' ' + y + ' ' + z);
      console.log("@@CURR COORDS: (" + this.player.pos.x + ',' + this.player.pos.y + ',' + this.player.pos.z + ')');
      this.clickX.start(this.mouseWindow.x, this.mouseWindow.y, 1);
      this.socket.send('FLASHTO ' + x + ' ' + y + ' ' + z);
    }
  }

  handleSceneClick() {
    this.raycaster.setFromCamera(this.mouse, this.camera);

    const intersects = this.raycaster.intersectObjects(this.collideableObjects, true);

    let groundFound = false;
    let otherAction = false;
    let x, y, z;
    for (let i = 0; i < intersects.length; i++) {
      console.log(intersects[i].object.name)
      if (intersects[i].object.name.startsWith('ground_Mazmorra_test_0') ||
          intersects[i].object.name.startsWith('walls_Mazmorra_test_13') ||
          intersects[i].object.name.startsWith('pillar_tops_Mazmorra_test_4') ||
          intersects[i].object.name.startsWith('pillars_Mazmorra_test_6')) {
        groundFound = true;
        x = intersects[i].point.x;
        y = intersects[i].point.y;
        z = intersects[i].point.z;
        break;
      }/* else if(!otherAction) {
        otherAction = true;
        if(intersects[i].object.name === 'Spartan_LP') {
          socket.send('ATTACK ' + intersects[i].object.playerID);
        }
      }*/
    }
    if (groundFound && !otherAction) {
      console.log("@@MOVING TO", x + ' ' + y + ' ' + z);
      console.log("@@CURR COORDS: (" + this.player.pos.x + ',' + this.player.pos.y + ',' + this.player.pos.z + ')');
      this.clickX.start(this.mouseWindow.x, this.mouseWindow.y, 0);
      this.socket.send('MOVETO ' + x + ' ' + y + ' ' + z);
    }
  }

  handleHUDElements() {
    this.hudBitmap.clearColor(0.0, 0.0, 0.0, 0.0);
    this.hudBitmap.clear(this.hudBitmap.COLOR_BUFFER_BIT);

    let clickedElement;
    let elementsAfterRemoval = null;
    for (const element of this.uiElements) {
      if (element.removed) {
        if (elementsAfterRemoval === null) {
          elementsAfterRemoval = [...this.uiElements];
        }
        elementsAfterRemoval.splice(elementsAfterRemoval.indexOf(element), 1);
        continue;
      }
      if (this.leftClicked) {
        if (element.intersects(this.mouseWindow)) {
          // we take the last element in this.uiElements that intersects
          clickedElement = element;
        }
      }
      element.draw(this.hudBitmap);
    }
    if (elementsAfterRemoval !== null) {
      this.uiElements = elementsAfterRemoval;
    }

    if (clickedElement && clickedElement.onMouseClick) {
      clickedElement.onMouseClick({ x: this.mouseWindow.x, y: this.mouseWindow.y });
      this.leftClicked = false;
    }
  }

  animate = () => {
    requestAnimationFrame(this.animate);

    if (this.loaded) {
      TWEEN.update();

      this.player.updater();

      this.updatePlayers();
      /*
            this.hudBitmap.clearRect(0, 0, this.width, this.height);
            this.hudBitmap.font = "Normal 40px Arial";
            this.hudBitmap.fillStyle = "rgba(245,245,245,0.75)";
            this.hudBitmap.fillText('SecretGame', 90, 40);
      */
      this.handleHUDElements();
      this.hudTexture.needsUpdate = true;

      if(this.leftClicked) {
        this.handleSceneLeftClick();
        this.leftClicked = false;
      } else if (this.clicked) {
        this.handleSceneClick();
        this.consumeClick();
      }

      this.player.hpBar.center();

      for (let [k, v] of this.players) {
        v.hpBar.center();
      }
      this.updateCamera();


      const mapWidth = 117.207;
      const mapHeight = 118.968;

      const gl = this.hudBitmap;

      const dx = this.player.pos.x - this.camera.position.x;
      const dy = this.player.pos.z - this.camera.position.z;
      const rotation = Math.atan2(-dy, dx);

      //this.minimapElement.shaderMaterial.uniforms.inTexCoord.value[0] = (this.player.pos.x + (mapWidth / 2)) / mapWidth * 1064;
      //this.minimapElement.shaderMaterial.uniforms.inTexCoord.value[1] = (-this.player.pos.z + (mapHeight / 2)) / mapHeight * 1080;
      //this.minimapElement.shaderMaterial.uniforms.rotation.value = (rotation - Math.PI / 2);
      this.minimapElement.inTexCoord[0] = (this.player.pos.x + (mapWidth / 2)) / mapWidth * 1064;
      this.minimapElement.inTexCoord[1] = (this.player.pos.z + (mapHeight / 2)) / mapHeight * 1080;
      this.minimapElement.rotation = (rotation - Math.PI / 2);

      this.clickX.draw(this.hudBitmap);

      if (this.debugMenu) {
        this.debugMenu.draw(this.hudBitmap);
      }
    }

    //this.renderer.setViewport(0, 0, this.width, this.height);
    this.renderer.render(this.scene, this.camera);
    //-0.5 + (width / 2)
    /*this.renderer.setViewport(0, 0, 500, 500);
    this.renderer.render(this.scene, this.mapCamera);

    this.renderer.setViewport(0, 0, this.width, this.height);*/
    this.renderer.render(this.sceneHUD, this.cameraHUD);

    this.stats.update();
  }

  handlePacket({ data: packet }) {
    const { command, data } = Client.parsePacket(packet);

    if (command === 'CREATE') {
      console.log('@@CREATE')
      const id = parseInt(data[1]);
      const x = parseFloat(data[2]);
      const y = parseFloat(data[3]);
      const z = parseFloat(data[4]);
      const dir = parseFloat(data[5]);
      const itemCount = parseInt(data[6]);

      let i = 7;
      this.inventory = new Array(32);
      for (let x = 0; x < itemCount; x++) {
        const id = parseInt(data[x * 2 + 7]);
        const slot = parseFloat(data[x * 2 + 8]);
        i += 2;
        this.inventory[slot] = new Item(id);
      }

      const mapCount = parseInt(data[i]);
      const map = [];
      i++;
      for(let j = 0; j < mapCount; j++) {
        const id = parseInt(data[i]);
        const x = parseFloat(data[i+1]);
        const y = parseFloat(data[i+2]);
        const z = parseFloat(data[i+3]);
        const rotation = parseFloat(data[i+4]);
        map.push({id, x, y, z, rotation});
        i += 5;
      }

      this.spawnPlayer(id, x, y, z, dir, map);

      this.loaded = true;
    } else if (command === 'DELITEM') {
      const slot = parseInt(data[1]);
      this.removeInventorySlot(slot);
    } else if (command === 'PUTITEM') {
      const id = parseInt(data[1]);
      const slot = parseInt(data[2]);
      this.putInventorySlot(slot, new Item(id));
    } else if (command === 'SWAPITEM') {
      const slot1 = parseInt(data[1]);
      const slot2 = parseInt(data[2]);
      const item1 = this.inventory[slot1];
      const item2 = this.inventory[slot2];
      this.removeInventorySlot(slot1);
      this.removeInventorySlot(slot2);
      this.putInventorySlot(slot1, item2);
      this.putInventorySlot(slot2, item1);
    } else if (command === 'FLASHTO') {
        const x = parseFloat(data[1]);
        const y = parseFloat(data[2]);
        const z = parseFloat(data[3]);
        const dir = parseFloat(data[4]);
  
        if(this.runningMoveTween) {
          this.runningMoveTween.stop();
          this.runningMoveTween = undefined;
        }

        this.player.enableAnim(Player.IDLE_ANIM);
        this.player.pos.x = x;
        this.player.pos.y = y;
        this.player.pos.z = z;
        this.player.model.rotation.y = dir;

        console.log('@@FLASHTO', x, y, z);
    } else if (command === 'FORCEMOVETO') {
      const x = parseFloat(data[1]);
      const y = parseFloat(data[2]);
      const z = parseFloat(data[3]);
      const dir = parseFloat(data[4]);

      this.player.pos.x = x;
      this.player.pos.y = y;
      this.player.pos.z = z;
      this.player.model.rotation.y = dir;

      console.log('@@FORCEMOVETO', x, y, z);
    } else if (command === 'MOVETO') {
      const pos = new THREE.Vector3(
        parseFloat(data[1]),
        parseFloat(data[2]),
        parseFloat(data[3])
      );
      const dir = parseFloat(data[4]);
      const speed = parseFloat(data[5]);

      console.log('@@MOVETO', pos.x, pos.y, pos.z, dir);
      this.player.enableAnim(Player.WALK_ANIM);

      this.player.model.rotation.y = dir;
      this.player.speed = speed;

      const coords = new THREE.Vector3(this.player.pos.x, this.player.pos.y, this.player.pos.z);

      if (this.runningMoveTween) {
        this.runningMoveTween.stop();
      }
      const movingTween = this.runningMoveTween = new TWEEN.Tween(coords)
        .to({ x: pos.x, y: pos.y, z: pos.z }, this.player.walkDelay(pos))
        .easing(TWEEN.Easing.Linear.None)
        .onUpdate(() => {
          this.camera.position.x += coords.x - this.player.pos.x;
          this.camera.position.y += coords.y - this.player.pos.y;
          this.camera.position.z += coords.z - this.player.pos.z;
          this.player.pos.copy(coords);
        })
        .onComplete(() => {
          if (movingTween === this.runningMoveTween) {
            this.runningMoveTween = undefined
          }
        })
        .start()
    } else if (command === 'MOVECOMPLETE') {
      this.player.enableAnim(Player.IDLE_ANIM);
    } else if (command === 'PLAYERMOVECOMPLETE') {
      const playerID = parseInt(data[1]);
      let player = this.players.get(playerID);

      if (player === undefined) {
        console.log('PANIC: Player undefined in playermovecomplete');
        return;
      }
      player.enableAnim(Player.IDLE_ANIM);
    } else if (command === 'PLAYERMOVE') {
      const playerID = parseInt(data[1]);
      const pos = new THREE.Vector3(
        parseFloat(data[2]),
        parseFloat(data[3]),
        parseFloat(data[4])
      );
      const dir = parseFloat(data[5]);
      const speed = parseFloat(data[6]);

      console.log('@@PLAYERMOVE', playerID, pos.x, pos.y, pos.z, dir, speed);

      let player = this.players.get(playerID);

      if (player === undefined) {
        console.log('PANIC: Player undefined in playermove');
        return;
      }

      player.speed = speed;
      player.enableAnim(Player.WALK_ANIM);

      const coords = new THREE.Vector3().copy(player.pos);

      if (player.walkTween) {
        player.walkTween.stop();
      }

      const playerWalkTween = player.walkTween = new TWEEN.Tween(coords)
        .to({ x: pos.x, y: pos.y, z: pos.z }, player.walkDelay(pos))
        .easing(TWEEN.Easing.Linear.None)
        .onUpdate(() => {
          player.pos.copy(coords);
        })
        .onComplete(() => {
          if (playerWalkTween === player.walkTween) {
            player.walkTween = undefined;
          }
        })
        .start()

      player.model.rotation.y = dir;
    } else if (command === 'PLAYERFLASHTO') {
      const id = parseFloat(data[1]);
      const x = parseFloat(data[2]);
      const y = parseFloat(data[3]);
      const z = parseFloat(data[4]);
      const dir = parseFloat(data[5]);

      const player = this.players.get(id);
      if(player === undefined) {
        console.log('PANIC: Player undefined in PLAYERFLASHTO');
        return;
      }

      if(player.moveTween) {
        player.moveTween.stop();
        player.moveTween = undefined;
      }

      player.enableAnim(Player.IDLE_ANIM);
      player.pos.x = x;
      player.pos.y = y;
      player.pos.z = z;
      player.model.rotation.y = dir;

      console.log('@@PLAYERFLASHTO', id, x, y, z);
    } else if (command === 'PLAYERS') {
      console.log('@@PLAYERS');
      this.playerIDsLastUpdate.length = 0;

      for (let i = 1; i < data.length; i += 6) {
        const playerID = parseInt(data[i]);
        const pos = new THREE.Vector3(
          parseFloat(data[i + 1]),
          parseFloat(data[i + 2]),
          parseFloat(data[i + 3])
        );
        const playerDir = parseFloat(data[i + 4]);
        const playerMoving = parseInt(data[i + 5]);

        this.playerIDsLastUpdate.push(playerID);

        let player = this.players.get(playerID);

        if (player === undefined) {
          player = new Player(this.playerModel, playerID, this.camera);

          player.pos.copy(pos);
          player.model.rotation.y = playerDir;

          this.scene.add(player.model);
          this.scene.add(player.hpBar.redMesh);
          this.scene.add(player.hpBar.greenMesh);

          this.collideableObjects.push(player.model);

          player.enableAnim(Player.IDLE_ANIM);

          this.players.set(playerID, player);

          continue;
        }

        const coords = new THREE.Vector3().copy(player.pos);

        const tween = new TWEEN.Tween(coords)
          .to({ x: pos.x, y: pos.y, z: pos.z }, player.walkDelay(pos))
          .easing(TWEEN.Easing.Linear.None)
          .onUpdate(() => {
            player.pos.copy(coords);
          })
          .onComplete(() => {
            player.enableAnim(Player.IDLE_ANIM);
          })
          .start()

        player.model.rotation.y = playerDir;

        if (playerMoving) {
          player.enableAnim(Player.WALK_ANIM);
        } else {
          player.enableAnim(Player.IDLE_ANIM);
        }
      }
    }
  }

  static loadModels() {
    const loader = new FBXLoader();

    const l = new THREE.TextureLoader();

    return new Promise((res, rej) => {
      l.load('/static/Sword_texture/Sword_texture.png', swordTexture => {
        l.load('/static/Shield_texture/Shield_texture.png', shieldTexture => {
          l.load('/static/Shield_texture/Shield_2.png', shieldTexture2 => {
            l.load('/static/Spartan_warrior_texture/Spartan_Warrior_Texture_.png', spTexture => {
              loader.load('/static/Spartan_Warrior.fbx', model => {
                model.traverse(function (child) {
                  if (child.name === 'Shield_LP') {
                    child.material[0].map = shieldTexture;
                    child.material[0].side = THREE.DoubleSide;
                    child.material[1].map = shieldTexture2;
                  } else if (child.name === 'Sword_LP') {
                    child.material.map = swordTexture;
                  } else if (child.name === 'Spartan_LP') {
                    child.material.map = spTexture;
                  }
                });

                const loadMapFile = (str, id) => new Promise((res, rej) => {
                  const gltfLoader = new GLTFLoader();
                  gltfLoader.load(`/static/maprooms/${str}.glb`, scene => res(scene));
               /*   const mtlLoader = new MTLLoader();
                  mtlLoader.load(`/static/maprooms/${str}.mtl`, mat => {
                    const objLoader = new OBJLoader();

                    objLoader.setMaterials(mat);
                    objLoader.load(`/static/maprooms/${str}.obj`, scene => {
                      res(scene);
                    });
                  });
                  */
                });

                const ground = Promise.all([
                  loadMapFile('room0', 0),
                  loadMapFile('room1', 1),
                  loadMapFile('room2', 2),
                  loadMapFile('room3', 3),
                  loadMapFile('room4', 4),
                ]);

                ground.then(terrain => {
                  const images = Promise.all([
                    '/static/inventory.png',
                    '/static/inventory-active.png',
                    '/static/levels.png',
                    '/static/levels-active.png',
                    '/static/inventory-panel.png',
                    '/static/levels-panel.png',
                    '/static/potion.png',
                    '/static/X.png',
                    '/static/green-X.png',
                    '/static/minimap.png',
                  ].map(url => new Promise((res, rej) => {
                    if (url === undefined) return Promise.resolve(null);
                    const image = new Image();
                    image.src = url;
                    image.addEventListener('load', () => {
                      res(image);
                    });
                  })));
                  images.then(sprites => {
                    new THREE.TextureLoader().load('/static/minimap.png', sprite => {
                      //sprites[9] = sprite;
                      res({ terrain, playerModel: model, sprites });
                    })
                  });
                });
              }, undefined, error => {
                rej(error);
              });
            });
          });
        });
      });
    });
  };
}

document.addEventListener('contextmenu', event => event.preventDefault());
document.addEventListener('DOMContentLoaded', () => {
  Client.loadModels()
    .then(({ terrain, playerModel, sprites }) => {

      const client = new Client(window.innerWidth, window.innerHeight);

      client.ground = terrain;

      console.log(client.ground);
      client.playerModel = playerModel;
      client.sprites = sprites;
      client.itemTextures = [client.sprites[6]];

      client.setupRenderer();
      client.setupHUD();
      client.setupControlsMenu();
      client.clickX = new ClickX(client.hudBitmap, {
        textures: [client.sprites[7], client.sprites[8]],
        program: client.PROG_TEXTURED_RECT,
      });

      //client.collideableObjects.push(client.ground);
      //client.scene.add(client.ground);

      var ambientLight = new THREE.AmbientLight(0x555555);
      client.scene.add(ambientLight);


      client.addLight()

      client.animate();

      window.addEventListener('mousedown', event => client.onMouseClick(event), false);
      window.addEventListener('mouseup', event => client.onMouseUp(event), false);
      window.addEventListener('mousemove', event => client.onMouseMove(event), false);
      window.addEventListener('resize', () => client.onResize());

      client.connect('ws://localhost:8000/ws', {
        onOpen: () => console.log('@@CONNECT'),
        onMessage: event => client.handlePacket(event)
      });
    }).catch(err => console.error(err));
});