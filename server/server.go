package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/arl/gogeo/f32/d3"
	"github.com/gorilla/websocket"
)

type Player struct {
	hub      *Hub
	conn     *websocket.Conn
	messages chan []byte
	send     chan []byte

	id                int
	loaded            bool
	x                 float64
	y                 float64
	z                 float64
	path              []d3.Vec3
	direction         float64
	pathIdx           int
	moving            bool
	needsMoveComplete bool
	lastDirection     float64
	lastMoveSent      time.Time
	lastWalkDistance  float64
	needsPlayerUpdate bool
	startStep         d3.Vec3
	endStep           d3.Vec3
	speed             float64

	items     []*Item
	itemCount int
}

type Item struct {
	id int
}

var (
	playerMutex = &sync.Mutex{}
)

func (p *Player) sendMove(x, y, z, direction, speed float64) {
	fmt.Printf("> MOVETO[%d] %f %f %f %f %f\n", p.id, x, y, z, direction, speed)
	p.send <- []byte(fmt.Sprintf("MOVETO %f %f %f %f %f", x, y, z, direction, speed))
}

func (p *Player) sendForceMove(x, y, z, direction float64) {
	p.send <- []byte(fmt.Sprintf("FORCEMOVETO %f %f %f %f", x, y, z, direction))
}

func (p *Player) sendLoad() {
	fmt.Println("> Sent load")
	str := &strings.Builder{}
	str.WriteString(fmt.Sprintf("CREATE %d %f %f %f %f %d", p.id, p.x, p.y, p.z, p.direction, p.itemCount))

	for i := 0; i < len(p.items); i++ {
		if p.items[i] != nil {
			str.WriteString(fmt.Sprintf(" %d %d", p.items[i].id, i))
		}
	}

	str.WriteString(fmt.Sprintf(" %d", level1.Size))
	level1.Traverse(func(m *MapRoom) {
		str.WriteString(fmt.Sprintf(" %d %f %f %f %f", m.id, m.x, m.y, m.z, m.rotation))
	})
	p.send <- []byte(str.String())
}

func (p *Player) processPackets() {
outer:
	for {
		select {
		case data0 := <-p.messages:
			data := strings.Split(string(data0), " ")
			command := data[0]
			if command == "MOVETO" {
				x, _ := strconv.ParseFloat(data[1], 64)
				y, _ := strconv.ParseFloat(data[2], 64)
				z, _ := strconv.ParseFloat(data[3], 64)
				p.moveTo(x, y, z)
			} else if command == "USEITEM" {
				slot, _ := strconv.ParseInt(data[1], 10, 32)
				p.useItem(int(slot))
			} else if command == "FLASHTO" {
				x, _ := strconv.ParseFloat(data[1], 64)
				y, _ := strconv.ParseFloat(data[2], 64)
				z, _ := strconv.ParseFloat(data[3], 64)
				p.flashTo(x, y, z)
			}
		default:
			break outer
		}
	}
}

func (p *Player) useItem(slot int) {
	if slot < 32 {
		if p.items[slot] != nil {
			p.items[slot] = nil
			p.send <- []byte(fmt.Sprintf("DELITEM %d", slot))
		}
	}
}

func (p *Player) movePlayer() {
	timeDiff := time.Since(p.lastMoveSent).Milliseconds()

	stepMultiplier := float32(float64(timeDiff) / (400 * (p.lastWalkDistance / p.speed)))

	dx := p.endStep.X() - p.startStep.X()
	p.x = float64(p.startStep.X() + dx*stepMultiplier)

	dy := p.endStep.Y() - p.startStep.Y()
	p.y = float64(p.startStep.Y() + dy*stepMultiplier)

	dz := p.endStep.Z() - p.startStep.Z()
	p.z = float64(p.startStep.Z() + dz*stepMultiplier)
}

func (p *Player) processMovement() {
	if p.moving {
		if len(p.path) == 0 {
			p.moving = false
			p.pathIdx = 0
		} else if p.path == nil {
			p.moving = false
			p.pathIdx = 0
		} else {

			if time.Since(p.lastMoveSent).Milliseconds() < int64(400*(p.lastWalkDistance/p.speed)) {
				if p.lastMoveSent.Nanosecond() > 0 {

					p.movePlayer()
				}

				return
			}

			if p.pathIdx >= len(p.path) {
				p.moving = false
				p.pathIdx = 0
				p.x = float64(p.endStep.X())
				p.y = float64(p.endStep.Y())
				p.z = float64(p.endStep.Z())
				p.send <- []byte("MOVECOMPLETE")
				for p1 := range p.hub.players {
					if p == p1 {
						continue
					}
					p1.send <- []byte(fmt.Sprintf("PLAYERMOVECOMPLETE %d", p.id))
				}
				return
			}

			t := p.path[p.pathIdx]

			p.lastWalkDistance = distance(p.x, p.y, p.z, float64(t.X()), float64(t.Y()), float64(t.Z()))
			if p.lastWalkDistance < 0.1 && len(p.path) > 1 {
				p.direction = p.lastDirection
			} else {
				p.direction = calculateDirection(p.x, p.z, float64(t.X()), float64(t.Z()))
				p.lastDirection = p.direction
			}

			p.startStep = d3.NewVec3XYZ(float32(p.x), float32(p.y), float32(p.z))
			p.endStep = d3.NewVec3XYZ(t.X(), t.Y(), t.Z())

			p.x = float64(t.X())
			p.y = float64(t.Y())
			p.z = float64(t.Z())

			p.sendMove(p.x, p.y, p.z, p.direction, p.speed)
			p.lastMoveSent = time.Now()
			p.pathIdx++

			for p1 := range p.hub.players {
				if p == p1 {
					continue
				}
				p1.send <- []byte(fmt.Sprintf("PLAYERMOVE %d %f %f %f %f %f", p.id, p.x, p.y, p.z, p.direction, p.speed))
			}
		}
	}
}

func (p0 *Player) processOtherPlayers() {
	if p0.needsPlayerUpdate {
		buf := &bytes.Buffer{}
		buf.WriteString("PLAYERS")
		movingPlayers := []*Player{}
		for p1 := range hub.players {
			if p1 == p0 {
				continue
			}
			var moving int
			if p1.moving {
				moving = 1
				movingPlayers = append(movingPlayers, p1)
			}
			buf.WriteString(fmt.Sprintf(" %d %f %f %f %f %d", p1.id, p1.x, p1.y, p1.z, p1.direction, moving))
		}

		p0.send <- buf.Bytes()
		for _, p := range movingPlayers {
			lastPoint := p.path[p.pathIdx-1]
			p0.send <- []byte(fmt.Sprintf("PLAYERMOVE %d %f %f %f %f %f", p.id, lastPoint.X(), lastPoint.Y(), lastPoint.Z(), p.direction, p.speed))
		}
		p0.needsPlayerUpdate = false
	}

}

func (p *Player) flashTo(x, y, z float64) {
	if p.moving {
		p.moveComplete()
	}
	dist := distance(p.x, p.y, p.z, x, y, z)
	if dist > 19 {
		return
	}
	dest := []float32{float32(x), float32(y), float32(z)}
	pt := level1Map.nearestPoint(dest)
	if pt == nil {
		return
	}
	direction := calculateDirection(p.x, p.z, float64(pt[0]), float64(pt[2]))
	p.x = float64(pt[0])
	p.y = float64(pt[1])
	p.z = float64(pt[2])
	fmt.Println("FLASHTO", pt[0], pt[1], pt[2])
	p.send <- []byte(fmt.Sprintf("FLASHTO %f %f %f %f", pt[0], pt[1], pt[2], direction))
	for p1 := range p.hub.players {
		if p == p1 {
			continue
		}
		p1.send <- []byte(fmt.Sprintf("PLAYERFLASHTO %d %f %f %f %f", p.id, pt[0], pt[1], pt[2], direction))
	}
}

func (p *Player) moveComplete() {
	p.send <- []byte("MOVECOMPLETE")
	for p1 := range p.hub.players {
		if p1 == p {
			continue
		}
		p1.send <- []byte(fmt.Sprintf("PLAYERMOVECOMPLETE %d", p.id))
	}
	p.moving = false
	p.path = nil
	p.pathIdx = 0
	p.lastMoveSent = time.Unix(0, 0)
}

func (p *Player) moveTo(x, y, z float64) {
	p.path = nil
	if p.x == x && p.y == y && p.z == z {
		p.needsMoveComplete = true
		p.moving = false
		p.pathIdx = 0
		return
	}
	p.pathIdx = 0
	var err error
	src := []float32{float32(p.x), float32(p.y), float32(p.z)}
	dest := []float32{float32(x), float32(y), float32(z)}
	p.path, err = level1Map.calcPath(src, dest)

	if err != nil {
		fmt.Printf("PATHING ERROR: %v\n", err)
		return
	}
	if len(p.path) < 1 {
		if p.moving {
			p.send <- []byte("MOVECOMPLETE")
			for p1 := range p.hub.players {
				if p1 == p {
					continue
				}
				p1.send <- []byte(fmt.Sprintf("PLAYERMOVECOMPLETE %d", p.id))
			}
			p.moving = false
			p.lastMoveSent = time.Unix(0, 0)
		}
		return
	}
	p.path = p.path[1:]

	p.moving = true
	p.lastMoveSent = time.Unix(0, 0)
}

func (p *Player) processLoad() {
	if !p.loaded {
		p.sendLoad()
		p.loaded = true
		p.needsPlayerUpdate = true
	}
}

func processPlayers() {
	playerMutex.Lock()
	for p := range hub.players {
		p.processLoad()
		p.processPackets()
		p.processMovement()
		p.processOtherPlayers()
	}
	playerMutex.Unlock()
}

func serverLoop() {
	for {
		//t := time.Now()
		processPlayers()
		//t2 := max(0, int(50-time.Since(t).Milliseconds()))
		time.Sleep(30 * time.Millisecond)
	}
}
