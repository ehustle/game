%module "recastdetour"
%{
#include "Recast.h"
#include "InputGeom.h"
#include "Sample.h"
#include "Sample_SoloMesh.h"
#include "DetourTileCache.h"
%}

%include "std_string.i"

// This will create 2 wrapped types in Go called
// "StringVector" and "ByteVector" for their respective
// types.
namespace std {
//   %template(SampleSoloMesh) Sample_SoloMesh;
}

%include "DetourTileCache.h"
%include "Recast.h"
%include "InputGeom.h"
%include "Sample.h"
%include "Sample_SoloMesh.h"
