//
// Copyright (c) 2009-2010 Mikko Mononen memon@inside.org
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//

#ifndef RECASTSAMPLESOLOMESH_H
#define RECASTSAMPLESOLOMESH_H

#include "Sample.h"
#include "DetourNavMesh.h"
#include "Recast.h"
#include "DetourTileCache.h"

class Sample_SoloMesh : public Sample
{
protected:
	bool m_keepInterResults;
	float m_totalBuildTimeMs;

	unsigned char* m_triareas;
	rcHeightfield* m_solid;
	rcCompactHeightfield* m_chf;
	rcContourSet* m_cset;
	rcPolyMesh* m_pmesh;
	rcConfig m_cfg;	
	rcPolyMeshDetail* m_dmesh;
	
	struct LinearAllocator* m_talloc;
	struct FastLZCompressor* m_tcomp;
	struct MeshProcess* m_tmproc;

	float m_tileSize;
	int m_maxTiles;
	int m_maxPolysPerTile;
	float m_cacheBuildTimeMs;
	int m_cacheCompressedSize;
	int m_cacheRawSize;
	int m_cacheLayerCount;
	int m_cacheBuildMemUsage;

	void cleanup();
		
public:
	class dtTileCache* m_tileCache;

	Sample_SoloMesh();
	virtual ~Sample_SoloMesh();
	
	virtual void handleSettings();
	virtual void handleMeshChanged(class InputGeom* geom);
	virtual bool handleBuild(class BuildSettings* buildSettings);
	virtual int exportAsObj();
	virtual void saveMesh();
	virtual int calcPath(const int maxSmooth, float src[3], float dest[3], float* smoothPath);
	virtual bool buildTileCache(class BuildSettings* buildSettings);
	virtual bool buildTileCache2(class BuildSettings* buildSettings);
	virtual void addObstacle(float* pos, float radius, float height, dtObstacleRef* ref);
	virtual void nearestPoint(float dest[3], float* point);
	virtual void saveAll(const char* path);
	virtual void loadAll(const char* path);
private:
	// Explicitly disabled copy constructor and copy assignment operator.
	Sample_SoloMesh(const Sample_SoloMesh&);
	Sample_SoloMesh& operator=(const Sample_SoloMesh&);
};


#endif // RECASTSAMPLESOLOMESHSIMPLE_H
