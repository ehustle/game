package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"math"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/g3n/engine/loader/obj"
	"github.com/g3n/engine/math32"
	"github.com/gorilla/mux"
)

var (
	templates *template.Template
	hub       *Hub

	level1Map *Map
	level1    *Level
)

type MapRoom struct {
	def         *MapRoomDefinition
	box         *math32.Box3
	x           float32
	y           float32
	z           float32
	rotation    float32
	orientation int
	width       float32
	height      float32
	id          int
}

type Entrance struct {
	X           float64
	Y           float64
	Width       float64
	Orientation int
	Occupied    bool
}

type MapRoomDefinition struct {
	Objpath   string
	Mtlpath   string
	Entrances []*Entrance
}

func copyV(dest *math32.Vector3, src *math32.Vector3) {
	dest.X = src.X
	dest.Y = src.Y
	dest.Z = src.Z
}

func cloneBox(box *math32.Box3) *math32.Box3 {
	newBMin := math32.NewVec3()
	newBMax := math32.NewVec3()

	copyV(newBMin, &box.Min)
	copyV(newBMax, &box.Max)

	return math32.NewBox3(newBMin, newBMax)
}

func (m *MapRoom) Clone() *MapRoom {
	entrances := make([]*Entrance, len(m.def.Entrances))
	for i := range entrances {
		entrances[i] = &Entrance{
			X:           m.def.Entrances[i].X,
			Y:           m.def.Entrances[i].Y,
			Orientation: m.def.Entrances[i].Orientation,
			Width:       m.def.Entrances[i].Width,
			Occupied:    m.def.Entrances[i].Occupied,
		}
	}
	def := &MapRoomDefinition{
		Objpath:   m.def.Objpath,
		Mtlpath:   m.def.Mtlpath,
		Entrances: entrances,
	}
	return &MapRoom{
		def:      def,
		box:      cloneBox(m.box),
		x:        m.x,
		y:        m.y,
		z:        m.z,
		width:    m.width,
		rotation: m.rotation,
		id:       m.id,
	}
}

func minf32(a, b float32) float32 {
	if a > b {
		return b
	}

	return a
}

func maxf32(a, b float32) float32 {
	if a < b {
		return b
	}

	return a
}

func growBox(a, b *math32.Box3) {
	bmin := a.Min
	bmax := a.Max

	bmin.X = minf32(bmin.X, b.Min.X)
	bmin.Y = minf32(bmin.Y, b.Min.Y)
	bmin.Z = minf32(bmin.Z, b.Min.Z)

	bmax.X = maxf32(bmax.X, b.Max.X)
	bmax.Y = maxf32(bmax.Y, b.Max.Y)
	bmin.Z = maxf32(bmin.Z, b.Min.Z)
}

func loadMapRoom(filePath string, id int) *MapRoom {
	f, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer f.Close()
	def := &MapRoomDefinition{}
	err = json.NewDecoder(f).Decode(&def)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// TODO figure out why this is necessary
	for _, d := range def.Entrances {
		d.Y = -d.Y
	}
	decoder, err1 := obj.Decode("maprooms/"+def.Objpath, "maprooms/"+def.Mtlpath)
	if err1 != nil {
		fmt.Println(err1)
		os.Exit(1)
	}
	var box *math32.Box3
	var width float32
	var height float32
	for _, o := range decoder.Objects {
		geo, err := decoder.NewGeometry(&o)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		bb := geo.BoundingBox()
		if box == nil {
			box = &bb
		} else {
			growBox(box, &bb)
		}
		width = box.Max.X - box.Min.X
		height = box.Min.Y - box.Min.Y
	}

	return &MapRoom{
		def:      def,
		box:      box,
		x:        0,
		y:        0,
		rotation: 0,
		width:    width,
		height:   height,
		id:       id,
	}
}

type Level struct {
	Root  *MapRoom
	Edges []*Level
	Size  int
}

func intersectAny(b *math32.Box3, root *Level) bool {
	if root.Root.box.IsIntersectionBox(b) {
		return true
	}
	for _, r := range root.Edges {
		intersect := intersectAny(b, r)
		if intersect {
			return true
		}
	}
	return false
}

func rotate(e *math32.Vector2, center *math32.Vector3, angle float32) *math32.Vector3 {
	s := float32(math.Sin(float64(angle)))
	c := float32(math.Cos(float64(angle)))
	eX := float32(e.X)
	eY := float32(e.Y)
	x := c*(eX-center.X) - s*(eY-center.Z) + center.X
	y := s*(eX-center.X) + c*(eY-center.Z) + center.Z
	return math32.NewVector3(x, 0, y)
}

func rotateBox(box *math32.Box3, center *math32.Vector3, angle float64) {
	out := []*float32{
		&box.Min.X, &box.Min.Y,
		&box.Max.X, &box.Max.Y,
	}
	s := float32(math.Sin(angle))
	c := float32(math.Cos(angle))
	for i := 0; i < 4; i += 2 {
		*out[i] = c*(*out[i]-center.X) - s*(*out[i+1]-center.Z) + center.X
		*out[i+1] = s*(*out[i]-center.X) + c*(*out[i+1]-center.Z) + center.Z
	}
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (m *MapRoom) Rotate(angle float32, orientationDiff int) *MapRoom {
	//x := (m.box.Max.X - m.box.Min.X) / 2
	//y := (m.box.Max.Z - m.box.Min.Z) / 2
	center := math32.NewVector3(0, 0, 0)
	for _, e := range m.def.Entrances {
		pt := rotate(math32.NewVector2(float32(e.X), float32(e.Y)), center, angle)
		//pt = math32.NewVector3(float32(e.X), 0, float32(e.Y))
		fmt.Println("ROTATED", e.X, e.Y, "to", pt.X, pt.Z)
		e.X = float64(pt.X)
		e.Y = float64(pt.Z)
		/*if m.id == 0 {
			e.X -= (float64(m.box.Max.X) - float64(m.box.Min.X))
		}*/
		fmt.Printf("setting orientation %d", e.Orientation)

		e.Orientation += orientationDiff
		//e.Orientation = 4 - e.Orientation
		e.Orientation %= 4
		/*if e.Orientation == 0 {
			e.Y += (float64(m.box.Max.Z) - float64(m.box.Min.Z)) / 2
		} else if e.Orientation == 1 {
			e.X -= (float64(m.box.Max.X) - float64(m.box.Min.X)) / 2
		} else if e.Orientation == 2 {
			e.Y -= (float64(m.box.Max.Z) - float64(m.box.Min.Z)) / 2
		} else if e.Orientation == 3 {
			e.X += (float64(m.box.Max.X) - float64(m.box.Min.X)) / 2
		}*/

		fmt.Printf(" -> %d\n", e.Orientation)
	}
	return m
}

func (l *Level) placeRoom(room *MapRoom, root *Level) *Level {
	targetRoom := l.Root

	// TODO: randomize access to entrances
	for _, targetEntrance := range targetRoom.def.Entrances {
		if targetEntrance.Occupied {
			fmt.Println("target occupied", targetEntrance.Orientation)
			continue
		}
		roomToAttach := room.Clone()
		// TODO: randomize access to entrances
		for _, roomToAttachEntrance := range roomToAttach.def.Entrances {
			to := targetEntrance.Orientation

			fmt.Println("~~~~~~~~~~~~~~~~~ADDING orientations ", roomToAttachEntrance.Orientation, "->", targetEntrance.Orientation)
			var orientationDiff int
			if to == 0 && roomToAttachEntrance.Orientation == 0 {
				orientationDiff = 2
			} else if to == 0 && roomToAttachEntrance.Orientation == 1 {
				orientationDiff = 1
			} else if to == 0 && roomToAttachEntrance.Orientation == 2 {
				orientationDiff = 2
			} else if to == 0 && roomToAttachEntrance.Orientation == 3 {
				orientationDiff = 3
			} else if to == 1 && roomToAttachEntrance.Orientation == 0 {
				orientationDiff = 3
			} else if to == 1 && roomToAttachEntrance.Orientation == 1 {
				orientationDiff = 2
			} else if to == 1 && roomToAttachEntrance.Orientation == 2 {
				orientationDiff = 1
			} else if to == 1 && roomToAttachEntrance.Orientation == 3 {
				orientationDiff = 0
			} else if to == 2 && roomToAttachEntrance.Orientation == 0 {
				orientationDiff = 0
			} else if to == 2 && roomToAttachEntrance.Orientation == 1 {
				// clockwise
				orientationDiff = 1
			} else if to == 2 && roomToAttachEntrance.Orientation == 2 {
				orientationDiff = 2
			} else if to == 2 && roomToAttachEntrance.Orientation == 3 {
				orientationDiff = 1
			} else if to == 3 && roomToAttachEntrance.Orientation == 0 {
				orientationDiff = 1
			} else if to == 3 && roomToAttachEntrance.Orientation == 1 {
				orientationDiff = 0
			} else if to == 3 && roomToAttachEntrance.Orientation == 2 {
				orientationDiff = 3
			} else if to == 3 && roomToAttachEntrance.Orientation == 3 {
				orientationDiff = 2
			}
			fmt.Println(">>", orientationDiff)
			rotation := float32(math.Pi/2) * float32(orientationDiff)

			center := math32.NewVector3(0, 0, 0)

			ptX := float32(roomToAttachEntrance.X)
			ptY := float32(roomToAttachEntrance.Y)

			rotated := rotate(math32.NewVector2(ptX, ptY), center, rotation)
			//rotated = math32.NewVector3(ptX, 0, ptY)

			fmt.Println(rotated)
			var offsetX float32
			var offsetY float32
			var doorWidth float32
			var doorHeight float32

			checkOrientation := targetEntrance.Orientation
			fmt.Println("@@", checkOrientation)
			if checkOrientation == 0 {
				offsetX = float32(targetEntrance.X) - rotated.X
				offsetY = (roomToAttach.box.Max.Z-roomToAttach.box.Min.Z)/2 + (targetRoom.box.Max.Z-targetRoom.box.Min.Z)/2
				/*offsetY = (roomToAttach.box.Max.Z - roomToAttach.box.Min.Z) / 2
				offsetX = (float32(targetEntrance.X) - rotated.X)
				*/
				fmt.Println("@@@@@@ MOVING SOUTH")
			} else if checkOrientation == 1 {
				offsetY = float32(targetEntrance.Y) - rotated.Z
				offsetX = -(roomToAttach.box.Max.Z-roomToAttach.box.Min.X)/2 + -(targetRoom.box.Max.Z-targetRoom.box.Min.X)/2
				/*offsetX = -(roomToAttach.box.Max.X - roomToAttach.box.Min.X) / 2
				offsetY = (float32(targetEntrance.Y) - rotated.Z)
				*/
				fmt.Println("@@@@@@ MOVING WEST", offsetY)
			} else if checkOrientation == 2 {
				offsetX = float32(targetEntrance.X) - rotated.X
				offsetY = -(roomToAttach.box.Max.Z-roomToAttach.box.Min.Z)/2 + -(targetRoom.box.Max.Z-targetRoom.box.Min.Z)/2
				/*offsetY = -(roomToAttach.box.Max.Z - roomToAttach.box.Min.Z) / 2
				offsetX = (float32(targetEntrance.X) - rotated.X)
				*/
				fmt.Println("@@@@@@ MOVING NORTH")
			} else if checkOrientation == 3 {
				offsetY = float32(targetEntrance.Y) - rotated.Z
				offsetX = (roomToAttach.box.Max.Z-roomToAttach.box.Min.X)/2 + (targetRoom.box.Max.Z-targetRoom.box.Min.X)/2

				/*
					offsetX = (roomToAttach.box.Max.X - roomToAttach.box.Min.X) / 2
					offsetY = (float32(targetEntrance.Y) - rotated.Z)
				*/
				fmt.Println("@@@@@@ MOVING EAST")
			}
			//doorWidth = float32(-targetEntrance.Width) - float32(roomToAttachEntrance.Width)

			//offsetX += float32(targetEntrance.X) - rotated.X
			//offsetY += float32(targetEntrance.Y) - rotated.Z

			fmt.Println("*&*&*&*&*", float32(targetEntrance.Y), rotated.Z)

			_ = doorWidth
			_ = doorHeight
			_ = offsetX
			_ = offsetY

			//rotated.X += offsetX
			//rotated.Z += offsetY

			//rotated.X += targetRoom.x
			//rotated.Z += targetRoom.z

			cx := targetRoom.x + offsetX
			cy := targetRoom.z + offsetY

			targetEntrance.Occupied = true
			roomToAttachEntrance.Occupied = true
			roomToAttach.x = cx
			roomToAttach.z = cy
			roomToAttach.rotation = -rotation

			return &Level{
				Root:  roomToAttach.Rotate(rotation, orientationDiff),
				Edges: make([]*Level, 0),
			}
		}
		break
	}

	return nil
}

func generateLevel(depth int, root *Level, rooms map[string]*MapRoom, end *MapRoom, level *Level) bool {
	if depth == 0 {
		l := level.placeRoom(end, root)
		if l != nil {
			level.Edges = append(level.Edges, l)
			root.Size++
			fmt.Println("rootSize=", root.Size)
			return true
		}
		fmt.Println("Failed to place end node in level")
		return false
	}
	var newRooms map[string]*MapRoom
	if depth == 3 {
		newRooms = map[string]*MapRoom{
			"room.json": rooms["room0.json"],
		}
	} else if depth == 2 {
		newRooms = map[string]*MapRoom{
			"room.json": rooms["room1.json"],
		}
	} else if depth == 1 {
		newRooms = map[string]*MapRoom{
			"room.json": rooms["room2.json"],
		}
	}
	// rooms being map makes this random selection
	for _, v := range newRooms {
		if l := level.placeRoom(v, root); l != nil {
			level.Edges = append(level.Edges, l)
			root.Size++

			return generateLevel(depth-1, root, rooms, end, l)
		}
	}

	return false
}

func (l *Level) String() string {
	buf := &strings.Builder{}
	buf.WriteString(fmt.Sprintf("(%d [%f %f %f %f]\n", l.Root.id, l.Root.x, l.Root.y, l.Root.z, l.Root.rotation))
	for _, r := range l.Edges {
		buf.WriteString(r.String())
	}
	buf.WriteString(")")
	return buf.String()
}

func (l *Level) Traverse(f func(m *MapRoom)) {
	f(l.Root)
	for _, e := range l.Edges {
		e.Traverse(f)
	}
}

func main() {
	rooms := map[string]*MapRoom{
		"room0.json": loadMapRoom("data/room0.json", 0),
		"room1.json": loadMapRoom("data/room1.json", 1),
		"room2.json": loadMapRoom("data/room2.json", 2),
	}

	start := loadMapRoom("data/room3.json", 3)
	end := loadMapRoom("data/room4.json", 4)

	level1 = &Level{Root: start, Edges: make([]*Level, 0), Size: 1}
	generateLevel(3, level1, rooms, end, level1)

	var err error
	templates, err = template.ParseFiles("../client/templates/index.html")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(templates.DefinedTemplates())

	level1Map, err = loadMapFile()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	/*pos := []float32{
		10.2100, 1.2084, -5.7395,
	}
	ref := uint(0)
	level1Map.navmesh.AddObstacle(&pos[0], 6, 6, &ref)
	*/
	hub = newHub()

	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("../client/static/"))))
	r.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go hub.run()
	go serverLoop()

	fmt.Println(srv.ListenAndServe())
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	templates.ExecuteTemplate(w, "index.html", nil)
}
