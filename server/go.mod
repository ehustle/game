module server

go 1.17

require (
	github.com/arl/go-detour v0.1.3
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
)

require (
	github.com/arl/assertgo v0.0.0-20180702120748-a1be5afdc871 // indirect
	github.com/arl/gogeo v0.0.0-20200405111831-9d419f5f7a90 // indirect
	github.com/arl/math32 v0.2.0 // indirect
	github.com/g3n/engine v0.2.0 // indirect
)
