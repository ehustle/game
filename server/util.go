package main

import (
	"fmt"
	"math"
	"os"
	recastdetour "server/recast-detour"

	"github.com/arl/gogeo/f32/d3"
)

type Map struct {
	//navmesh *detour.NavMesh
	navmesh recastdetour.Sample_SoloMesh
}

func max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func calculateDirection(x1, y1, x2, y2 float64) float64 {
	dx := x2 - x1
	dy := y2 - y1

	return math.Atan2(-dy, dx) + math.Pi*0.5
}

func distance(x1, y1, z1, x2, y2, z2 float64) float64 {
	dx := x1 - x2
	dy := y1 - y2
	dz := z1 - z2
	return math.Sqrt(float64(dx*dx + dy*dy + dz*dz))
}

func loadMapFile() (*Map, error) {
	/*f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	m := &Map{}
	m.navmesh, err = detour.Decode(f)
	if err != nil {
		return nil, err
	}
	return m, nil*/
	ctx := recastdetour.NewRcContext()
	geom := recastdetour.NewInputGeom()

	ok := geom.Load(ctx, "data/world.obj")
	if !ok {
		fmt.Println("failed")
		return nil, fmt.Errorf("loading map object failed")
	}
	m := &Map{}
	m.navmesh = recastdetour.NewSample_SoloMesh()
	settings := recastdetour.NewBuildSettings()
	settings.SetCellSize(0.3)
	settings.SetCellHeight(0.2)
	settings.SetAgentHeight(2)
	settings.SetAgentRadius(0.3)
	settings.SetAgentMaxClimb(0.4)
	settings.SetAgentMaxSlope(45)
	settings.SetRegionMinSize(8)
	settings.SetRegionMergeSize(20)
	settings.SetEdgeMaxLen(12)
	settings.SetEdgeMaxError(1.3)
	settings.SetVertsPerPoly(6)
	settings.SetDetailSampleDist(6)
	settings.SetDetailSampleMaxError(1)
	settings.SetPartitionType(int(recastdetour.SAMPLE_PARTITION_LAYERS))

	m.navmesh.SetContext(settings)
	m.navmesh.HandleMeshChanged(geom)
	if !m.navmesh.BuildTileCache(settings) {
		fmt.Println("Failed to build tile cache")
		os.Exit(1)
	}
	//m.navmesh.SaveAll("nav.bin")
	//os.Exit(0)
	//m.navmesh.HandleBuild(settings)
	/*m := &Map{}
	m.navmesh = recastdetour.NewSample_SoloMesh()
	m.navmesh.LoadAll("nav.bin")
	*/
	return m, nil
}

/*func (m *Map) calcStraightPath(src, dest d3.Vec3) ([]d3.Vec3, error) {
	st, q := detour.NewNavMeshQuery(m.navmesh, 1000)
	if st != detour.Success {
		return nil, fmt.Errorf("NOT SUCCESFFUL 1")
	}

	extent := d3.NewVec3XYZ(100, 100, 100)

	f1 := detour.NewStandardQueryFilter()

	st, srcRef, src0 := q.FindNearestPoly(src, extent, f1)
	if st != detour.Success {
		return nil, fmt.Errorf("NOT SUCCESFFUL 2")
	}

	st, destRef, dest0 := q.FindNearestPoly(dest, extent, f1)
	if st != detour.Success {
		return nil, fmt.Errorf("NOT SUCCESFFUL 3")
	}
	var (
		pathCount int
	)

	fmt.Println(srcRef, destRef, src0, dest0)
	path := make([]detour.PolyRef, 100)
	pathCount, st = q.FindPath(srcRef, destRef, src0, dest0, f1, path)

	if st != detour.Success {
		return nil, fmt.Errorf("NOT SUCCESFFUL 4")
	}

	var (
		straightPath      []d3.Vec3
		straightPathFlags []uint8
		straightPathRefs  []detour.PolyRef
		straightPathCount int
		maxStraightPath   int32
	)
	// slices that receive the straight path
	maxStraightPath = 100
	straightPath = make([]d3.Vec3, maxStraightPath)
	for i := range straightPath {
		straightPath[i] = d3.NewVec3()
	}
	straightPathFlags = make([]uint8, maxStraightPath)
	straightPathRefs = make([]detour.PolyRef, maxStraightPath)

	// (-2.395745,5.2,1.334207) error coords
	straightPathCount, st = q.FindStraightPath(src0, dest0, path[:pathCount], straightPath, straightPathFlags, straightPathRefs, 0)
	if detour.StatusFailed(st) {
		return nil, fmt.Errorf("query.FindStraightPath failed with 0x%x", st)
	}

	//if (straightPathFlags[0] & detour.StraightPathStart) == 0 {
	//	return nil, fmt.Errorf("straightPath start is not flagged StraightPathStart")
	//}

	if (straightPathFlags[straightPathCount-1] & detour.StraightPathEnd) == 0 {
		return nil, fmt.Errorf("straightPath end is not flagged StraightPathEnd")
	}

	return straightPath[:straightPathCount], nil
}*/

func (m *Map) calcPath(src, dest []float32) ([]d3.Vec3, error) {
	fmt.Println("calcpath from", src, dest)
	maxSmooth := 512
	path := make([]float32, maxSmooth*3)
	count := m.navmesh.CalcPath(maxSmooth, &src[0], &dest[0], &path[0])
	pathV := []d3.Vec3{}

	for i := 0; i < count; i++ {
		pathV = append(pathV, d3.NewVec3XYZ(path[i*3], path[i*3+1], path[i*3+2]))
	}

	return pathV, nil
}

func (m *Map) nearestPoint(dest []float32) []float32 {
	pt := make([]float32, 3)
	m.navmesh.NearestPoint(&dest[0], &pt[0])
	if pt[0] == 0 && pt[1] == 0 && pt[2] == 0 {
		return nil
	}
	return pt
}
